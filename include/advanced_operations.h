#ifndef ADVANCED_OPERATIONS_H
#define ADVANCED_OPERATIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/matrix.h"
#include "../include/operations.h"

Matrix extraction (Matrix a, int i, int j) ;
E determinant (Matrix a, int n) ;
Matrix triangular (Matrix a) ;
E optimized_determinant(Matrix a) ;
int rank (Matrix a) ;
Matrix inverse (Matrix a) ;
void LU_decomposition(Matrix a) ;

#endif
