#ifndef SYSTEM_SOLVER_H
#define SYSTEM_SOLVER_H

#include "../include/matrix.h"

void swap_lines(Matrix a, Matrix b, int i, int j) ;
int partial_pivot_choice(Matrix a, int i) ;
void add_multiple (Matrix a, Matrix b, int line1, int line2, E alpha) ;
void print_system(Matrix a, Matrix b) ;
void print_solution (Matrix x) ;
void system_triangularisation(Matrix a, Matrix b) ;
void raising (Matrix a, Matrix b, Matrix x) ;
int check_system (Matrix a, Matrix b) ;
void solve_system(Matrix a, Matrix b) ;
#endif
