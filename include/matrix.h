#ifndef MATRIX_H
#define MATRIX_H

typedef float E ;

typedef struct matrix
{
  E** mat ;
  int nb_rows ;
  int nb_columns ;
} Matrix ;

Matrix newMatrix(int nb_rows, int nb_columns) ;
void fill_matrix(Matrix a, E val) ;
void deleteMatrix(Matrix a) ;
E getElt(Matrix a, int rown, int column) ;
void setElt(Matrix a, int row, int column, E val) ;
void print_matrix(Matrix a) ;
Matrix identity (int size) ;

#endif
