#ifndef MAIN_BACKUP_FUNCTIONS
#define MAIN_BACKUP_FUNCTIONS

#include "../include/matrix.h"

Matrix setMatrixA() ;
Matrix setMatrixB() ;
Matrix setMatrixC() ;
Matrix setMatrixD() ;
Matrix setMatrixE() ;
Matrix setMatrixF() ;
void test_Symetric(Matrix a) ;
void test_Square(Matrix a) ;

#endif
