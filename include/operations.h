#ifndef OPERATIONS_H
#define OPERATIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/matrix.h"

Matrix error_matrix(Matrix a) ;
int sameSize (Matrix a, Matrix b) ;
int my_equality (E val_1, E val_2) ;
int matrix_equality(Matrix a, Matrix b) ;
int isSquare (Matrix a) ;
int isSymetric (Matrix a) ;
int checkMultiplication (Matrix a, Matrix b) ;
Matrix transpose (Matrix a) ;
Matrix copyMatrix (Matrix a) ;
Matrix addition (Matrix a, Matrix b) ;
Matrix multiplication (Matrix a, Matrix b) ;
Matrix mult_scalar (Matrix a, E alpha) ;
#endif
