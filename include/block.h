#ifndef BLOCK_H
#define BLOCK_H

#include "../include/matrix.h"


int is_contained_between(int x, int a, int b) ;
int zone_getblock (int current_row, int current_column, int block_beginning_row, int block_beginning_column, int block_ending_row, int block_ending_column) ;
int zone_block (int current_row, int current_column, int block_beginning_row, int block_beginning_column, Matrix b) ;
Matrix setMatrixBlock (Matrix a, int row, int column, Matrix b) ;
int checkBlock (int block_beginning_row, int block_beginning_column, int block_ending_row, int block_ending_column) ;
Matrix getMatrixBlock (Matrix a, int block_beginning_row, int block_beginning_column, int block_ending_row, int block_ending_column) ;
#endif
