# Matrix Manipulation

This program makes several operations using matrixes.

  Transpose
  Addition
  Multiplication
  Multiplication by a scalar
  Set/Get Block
  Determinant
  Rank
  Linear System Solver
  Invert
  LU Decomposition

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

Launch the Makefile and run bin/matrix 
