CC = gcc
CFLAGS = -Wall
LINKER = gcc -o
LFLAGS = -Wall -I.
LIB = -lm

TARGET=matrix

#Paths
OBJDIR = obj
SRCDIR = src
INCDIR = include
BINDIR = bin
SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(SRCDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
rm = rm -f

$(BINDIR)/$(TARGET): $(OBJECTS)
	@$(LINKER) $@ $(LFLAGS) $(OBJECTS) $(LIB)

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -c $< -o $@ $(LIB)

.PHONY: clean
clean:
	@$(rm) $(OBJECTS)

.PHONY: archive
archive :
	@tar -f projet_ALA_ABDOUROIHAMANE.tar.gz -cvz $(SRCDIR) $(INCDIR) $(BINDIR) $(OBJDIR) Makefile 1> /dev/null
