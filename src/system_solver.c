#include <stdio.h>
#include <stdlib.h>
#include "../include/system_solver.h"
#include "../include/matrix.h"
#include "../include/operations.h"
#define TRUE (1==1)
#define FALSE (1==0)

//Swap_lines : swaps the i and the j th line of Matrix a and Matrix b

void swap_lines(Matrix a, Matrix b, int i, int j)
{
    int k ;
    E tmp, val ;

    for (k = 1 ; k <= a.nb_columns ; k++)
    {
    	tmp = getElt(a,i,k) ;
    	val = getElt(a,j,k) ;

    	setElt(a,i,k,val) ;
    	setElt(a,j,k,tmp) ;
    }

    tmp = getElt(b,i,1) ;
    val = getElt(b,j,1) ;

    setElt(b,i,1,val) ;
    setElt(b,j,1,tmp) ;

    return ;
}

int partial_pivot_choice(Matrix a, int i)
{
    int p = i ;
    int j ;
    E val = abs(getElt(a,i,i)) ;
    E t ;

    for (j = i+1 ; j <= a.nb_rows ; j++)
    {
      t = abs(getElt(a,j,i)) ;

      if (t > val)
      {
        p = j ;
        val = t ;
      }
    }
    return p ;
}

//add_multiple : L1 = L1 + alpha*L2 on both Matrixes
void add_multiple (Matrix a, Matrix b, int line1, int line2, E alpha)
{
	int k ;
	E val ;

	for (k = 1 ; k <= a.nb_rows ; k++)
	{
		val = getElt(a,line1,k) ;
		val+= alpha*getElt(a,line2,k) ;
		setElt(a,line1,k,val) ;
	}

	val = getElt(b,line1,1) ;
	val+=alpha*getElt(b,line2,1) ;
	setElt(b,line1,1,val) ;

  return ;
}

void print_system(Matrix a, Matrix b)
{
	int i,j ;
	char letter ;
  E val ;

	for (i = 1 ; i <= a.nb_rows ; i++)
	{
		switch(i)
		{
				case 1 : letter = 'X' ; break ;
				case 2 : letter = 'Y' ; break ;
				case 3 : letter = 'Z' ; break ;
				default : break ;
		}

		printf("(\t");

		for (j = 1 ; j <= a.nb_rows ; j++)
		{
      val = getElt(a,i,j) ;
			printf("%.2f\t",val);
		}

		printf(" ) (%c) =",letter);

    val = getElt(b,i,1) ;
		printf(" %.2f \n\n",val);
	}

	return ;
}

void print_solution (Matrix x)
{
	int i ;
	char letter ;
  E val ;
	printf("System solution\n");

	for (i = 1 ; i <= x.nb_rows; i++)
	{
    val = getElt(x,i,1) ;

		switch(i)
		{
				case 1 : letter = 'X' ; break ;
				case 2 : letter = 'Y' ; break ;
				case 3 : letter = 'Z' ; break ;
				default : break ;
		}

		printf("%c = ",letter);
		printf("%.2f\n\n",val);
	}
}

// Triangularisation : Makes a triagular and makes the same calculations on b

void system_triangularisation(Matrix a, Matrix b)
{
    int i,j ;
    int n = a.nb_rows ;
    E val  ;

    for (i = 1 ; i <= (n-1) ; i++)
    {
        j = partial_pivot_choice(a,i) ;

        if (getElt(a,j,i) == 0.)
        {
        	fprintf(stderr, "\nError : Pivot is null.\n");
        	return ;
        }

        swap_lines(a,b,i,j) ;

        for (j = i+1 ; j <= a.nb_rows ; j++)
        {
        	val = -getElt(a,j,i)/getElt(a,i,i) ;
        	add_multiple(a,b,j,i,val) ;
        }

    }
}

/*  Raising
*   We can calculate the result from the last line of the matrix
*   Then, we can calculate the result from the last but not least line
*   Et caetera until the first line.
*/
void raising (Matrix a, Matrix b, Matrix x)
{
	int i, j ;
  int n = a.nb_rows ;
	E val ;

  	for(i = n ; i >= 1 ; i--)
  	{
  		val = getElt(b,i,1) ;
    	setElt(x,i,1,val) ;

    	for(j = (i+1); j <= n ; j++)
    	{
    		val = getElt(x,i,1) ;
    		val-= getElt(a,i,j)*getElt(x,j,1) ;
    		setElt(x,i,1,val) ;
    	}

    val = getElt(x,i,1) ;
    val/=getElt(a,i,i) ;
    setElt(x,i,1,val) ;
  }
}

// Check system : checks if the system is well written

int check_system (Matrix a, Matrix b)
{
  int bol = TRUE ;

  if ((isSquare(a) == FALSE) || (b.nb_rows != a.nb_columns) || (b.nb_columns != 1))
  {
    bol = FALSE ;
  }

  return bol ;
}

void solve_system(Matrix a, Matrix b)
{
  Matrix copy_a = copyMatrix(a) ;
  Matrix copy_b = copyMatrix(b) ;
  Matrix x = newMatrix(a.nb_rows,1) ;

  if (check_system(a,b) == FALSE)
  {
    fprintf(stderr, "Incorrect system\n");
  }

  else
  {
    print_system(copy_a,copy_b) ;
  	system_triangularisation(copy_a,copy_b) ;
    raising(copy_a,copy_b,x) ;
    print_solution(x) ;
  }

  deleteMatrix(copy_a) ;
  deleteMatrix(copy_b) ;
  deleteMatrix(x) ;
  return ;
}
