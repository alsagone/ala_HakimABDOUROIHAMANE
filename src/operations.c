#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/matrix.h"
#include "../include/operations.h"

#define TRUE (1==1)
#define FALSE (1==0)

//Return a matrix full of (-1) to indicate that the function lead to an error
Matrix error_matrix(Matrix a)
{
  Matrix err = newMatrix(a.nb_rows,a.nb_columns) ;
  fill_matrix(err,-1.) ;
  return err ;
}

int sameSize (Matrix a, Matrix b)
{
  int bol = TRUE ;

  if ((a.nb_rows != b.nb_rows) || (a.nb_columns != b.nb_columns))
  {
    bol = FALSE ;
  }

  return bol ;
}

int my_equality (E val_1, E val_2)
{
  int bol = TRUE ;
  E margin = 0.0000000001 ;

  if(abs(val_1-val_2) > margin) {bol = FALSE ;}

  return bol ;
}

int matrix_equality(Matrix a, Matrix b)
{
  int bol = TRUE ;
  int i, j ;
  E val_1, val_2 ;

  if (sameSize(a,b) == FALSE)
  {
    bol = FALSE ;
  }

  else
  {
    for (i = 1 ; i <= a.nb_rows ; i++)
    {
      for (j = 1 ; j <= a.nb_columns ; j++)
      {
        val_1 = getElt(a,i,j) ;
        val_2 = getElt(b,i,j) ;
        bol = my_equality(val_1,val_2) ;

        if (bol == FALSE) {break ;}
      }
    }
  }

  return bol ;
}

int isSquare (Matrix a)
{
  return(a.nb_rows == a.nb_columns)?TRUE:FALSE ;
}

int isSymetric (Matrix a)
{
  int i, j ;
  int bol = TRUE ;
  E val_1, val_2 ;

  if (isSquare(a) == FALSE)
  {
    bol = FALSE ;
  }
  else
  {
    for (i = 1 ; i <= a.nb_rows ; i++)
  	{
  		for (j = 1 ; j <= a.nb_columns ; j++)
  		{
  			val_1 = getElt(a,i,j) ;
  			val_2 = getElt(a,j,i) ;

  			if ((my_equality(val_1,val_2) == FALSE) && (i != j))
  			{
  				bol = FALSE ;
          break ;
  			}
  		}
    }
	}

	return bol ;
}

int checkMultiplication (Matrix a, Matrix b)
{
  return (a.nb_columns == b.nb_rows)?TRUE:FALSE ;
}

Matrix transpose (Matrix a)
{
  int i,j ;
  E val ;
  Matrix t = newMatrix(a.nb_columns,a.nb_rows) ;

  for (i = 1 ; i <= a.nb_rows ; i++)
  {
    for (j = 1 ; j <= a.nb_columns ; j++)
    {
      val = getElt(a,i,j) ;
      setElt(t,j,i,val) ;
    }
  }

  return t ;
}

Matrix copyMatrix (Matrix a)
{
  //transpose(transpose(A)) = A

  Matrix t = transpose(a) ;
  Matrix c = transpose(t) ;
  deleteMatrix(t) ;
  return c ;
}

Matrix addition (Matrix a, Matrix b)
{
	int i, j ;
	E val ;
  Matrix result = newMatrix(a.nb_rows, a.nb_columns) ;

	if (sameSize(a,b) == FALSE)
	{
		fprintf(stderr,"Addition impossible\n");
    result = error_matrix(result) ;
	}

  else
  {
    for (i = 1 ; i <= a.nb_rows; i++)
    {
    	for (j = 1 ; j <= a.nb_columns ; j++)
    	{
    		val = getElt(a,i,j) + getElt(b,i,j) ;
    		setElt(result,i,j,val) ;
    	}
    }
  }

  return result ;
}

Matrix multiplication (Matrix a, Matrix b)
{
  int i, j, k ;
  E val ;
  Matrix result = newMatrix(a.nb_rows,b.nb_columns) ;

  if (checkMultiplication(a,b) == FALSE)
  {
    fprintf(stderr, "Multiplication impossible.\n");
    result = error_matrix(result) ;
  }

  else
  {
    for (i = 1 ; i <= a.nb_rows ; i++)
    {
      for (j = 1 ; j <= b.nb_columns ; j++)
      {
        setElt(result,i,j,0.) ;
        for (k = 1 ; k <= b.nb_rows ; k++)
        {
          val = getElt(result,i,j) ;
          val+=getElt(a,i,k)*getElt(b,k,j) ;
          setElt(result,i,j,val) ;
        }
      }
    }
  }

  return result ;
}

Matrix mult_scalar (Matrix a, E alpha)
{
  int i, j ;
  E val ;
  Matrix result = newMatrix(a.nb_rows,a.nb_columns) ;

  for (i = 1 ; i <= a.nb_rows ; i++)
  {
    for (j = 1 ; j <= a.nb_columns ; j++)
    {
      val = getElt(a,i,j) * alpha ;
      setElt(result,i,j,val) ;
    }
  }

  return result ;
}
