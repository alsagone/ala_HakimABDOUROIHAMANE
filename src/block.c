#include <stdio.h>
#include <stdlib.h>
#include "../include/matrix.h"
#include "../include/operations.h"

#define TRUE (1==1)
#define FALSE (1==0)

int is_contained_between(int x, int a, int b) //Test a <= x <= b
{
  int bol = FALSE ;

  if ( (a <= x) && (x <= b) )
  {
    bol = TRUE ;
  }

  return bol ;
}

// zone_getblock : returns TRUE if the current_row and the current_column are in the block we want to get
int zone_getblock (int current_row, int current_column, int block_beginning_row, int block_beginning_column, int block_ending_row, int block_ending_column)
{
  int bol = FALSE ;
  int check_row = is_contained_between(current_row, block_beginning_row, block_ending_row) ;
  int check_column = FALSE ;

  if (block_beginning_column == block_ending_column)
  {
    if (current_column <= block_ending_column)
    {
      check_column = TRUE ;
    }
  }

  else
  {
    check_column = is_contained_between(current_column, block_beginning_column, block_ending_column) ;
  }


  if ((check_row == TRUE) && (check_column == TRUE))
  {
    bol = TRUE ;
  }

  return bol ;
}

// zone_getblock : returns TRUE if the current_row and the current_column are in the block zone
int zone_block (int current_row, int current_column, int block_beginning_row, int block_beginning_column, Matrix b)
{
  int block_ending_row = block_beginning_row + b.nb_rows - 1 ;
  int block_ending_column = block_beginning_column + b.nb_columns - 1 ;

  int check_row = is_contained_between(current_row, block_beginning_row, block_ending_row) ;
  int check_column = is_contained_between(current_column, block_beginning_column, block_ending_column) ;

  int bol = FALSE ;

  if ((check_row == TRUE) && (check_column == TRUE))
  {
    bol = TRUE ;
  }

  return bol ;
}

Matrix setMatrixBlock (Matrix a, int row, int column, Matrix b)
{
  // Le bloc doit être introduit au bon endroit --> ne pas déborder

  // a and b must be square
  if (isSquare(a) == FALSE)
  {
    fprintf(stderr, "Can't put a block in a non-square matrix.\n");
    return a ;
  }

  if (isSquare(b) == FALSE)
  {
    fprintf(stderr, "Block is not square.\n");
    return a ;
  }

  //The block needs to be introduced at the right place in order to have enough space
  if ((row + b.nb_rows - 1 > a.nb_rows) || (column + b.nb_columns - 1 > a.nb_columns))
  {
    fprintf(stderr, "Wrong block coordinates.\n");
    return a ;
  }

  int i, j ;
  Matrix a_block = newMatrix(a.nb_rows,a.nb_columns) ;
  E val ;

  int offset_block_row = 1 - row ;
  int offset_block_column = 1 - column ;

  for (i = 1 ; i <= a_block.nb_rows ; i++)
  {
    for (j = 1 ; j <= a_block.nb_columns ; j++)
    {
      if (zone_block(i,j,row,column,b) == TRUE)
      {
        val = getElt(b,i+offset_block_row,j+offset_block_column) ;
      }

      else
      {
        val = getElt(a,i,j) ;
      }

      setElt(a_block,i,j,val) ;
    }
  }

  return a_block ;

}

int checkBlock (int block_beginning_row, int block_beginning_column, int block_ending_row, int block_ending_column)
{
  /*	Before trying to extract a block, we need to check if the block is square
  *
  *		For example, for a block from (1,2) to (3,4)
  *		We know that 1 + 2 = 3 and 2 + 2 = 4 -> the block has 2 rows and 2 colums.
  *		Therefore, the block is square.
  *
  *		Generalization : for a block from (a,b) to (c,d)
  *		If the block is square, there is an integer x as a + x = c and b + x = d
  *		It is sufficient to calculate x = c - a and check if b + x = d to determine if the block is square .
  *
  *		When b = d : we need to check if the block has the same number of lines as the "column number"
  *
  *		The block from (1,3) to (3,3) has 3 (=3-1+1) lines so it is square.

  *		Generalization : the block from (a,b) to (c,b) is square if c - a + 1 = b.
  */

  int bol = FALSE ;
  int x ;

  if (block_beginning_column == block_ending_column)
  {
    x = block_ending_row - block_beginning_row + 1 ;

    if (x == block_ending_column)
    {
      bol = TRUE ;
    }
  }

  else
  {
    x = block_ending_row - block_beginning_row ;

    if (block_beginning_column + x == block_ending_column)
    {
      bol = TRUE ;
    }
  }

  return bol ;
}

Matrix getMatrixBlock (Matrix a, int block_beginning_row, int block_beginning_column, int block_ending_row, int block_ending_column)
{

  if (checkBlock(block_beginning_row, block_beginning_column, block_ending_row, block_ending_column) == FALSE)
  {
    fprintf(stderr, "Wrong block coordinates.\n");
    return a ;
  }

  int b_size, i, j ;
  E val ;
  Matrix block ;

  if (block_beginning_column == block_ending_column)
  {
    b_size = block_ending_column ;
  }
  else
  {
    b_size = block_ending_column - block_beginning_column + 1 ;
  }

  block = newMatrix(b_size,b_size) ;

  for (i = 1 ; i <= a.nb_rows ; i++)
  {
    for (j = 1 ; j <= a.nb_columns ; j++)
    {
      if (zone_getblock(i,j,block_beginning_row, block_beginning_column, block_ending_row, block_ending_column) == TRUE)
      {
        val = getElt(a,i,j) ;
        setElt(block,i,j-b_size+1,val) ;
      }
    }
  }

  return block ;
}
