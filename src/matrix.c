#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/matrix.h"

#define TRUE (1==1)
#define FALSE (1==0)


//Basic operations to manipulate matrixes

Matrix newMatrix(int nb_rows, int nb_columns)
{
	Matrix a ;
  int i ;

  a.mat = (E**) malloc (nb_rows*((sizeof(E*)))) ;
  a.nb_rows = nb_rows ;
  a.nb_columns = nb_columns ;

  for (i = 0 ; i < nb_rows ; i++)
  {
      a.mat[i] = (E*) malloc (nb_columns*(sizeof(E))) ;
  }

  if (a.mat==NULL)
  {
    fprintf(stderr, "Fail malloc\n");
    exit(1) ;
  }

  return a ;
}

void fill_matrix(Matrix a, E val)
{
  int i, j ;

  for (i = 1 ; i <= a.nb_rows ; i++)
  {
    for (j = 1 ; j <= a.nb_columns ; j++)
    {
      setElt(a,i,j,val) ;
    }
  }

  return ;
}

void deleteMatrix (Matrix a)
{
  int i ;

  for (i = 0 ; i < a.nb_rows ; i++)
  {
    free(a.mat[i]) ;
  }

  free(a.mat) ;

  return ;
}

E getElt(Matrix a, int row, int column)
{
  E val = a.mat[row-1][column-1] ;
  return val ;
}

void setElt(Matrix a, int row, int column, E val)
{
	a.mat[row-1][column-1] = val ;
	return ;
}

void print_matrix(Matrix a)
{
  //Display is limited to 2 digits after decimal
	printf("\n");
  int i,j ;
  E val ;

  for (i = 1 ; i <= a.nb_rows ; i++)
  {
    for (j = 1 ; j <= a.nb_columns ; j++)
    {
      val = getElt(a,i,j) ;
      printf("%.2f\t",val);
    }
    printf("\n");
  }

	printf("\n");
  return ;
}

Matrix identity (int size)
{
	Matrix id = newMatrix(size,size) ;
	int i, j ;
	E val ;

	for (i = 1 ; i <= size ; i++)
	{
		for (j = 1 ; j <= size ; j++)
		{
			val = 0. ;
			if (i == j) {val = 1. ;}
			setElt(id,i,j,val) ;
		}
	}

	return id ;
}
