#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/matrix.h"
#include "../include/operations.h"
#include "../include/advanced_operations.h"

#define TRUE (1==1)
#define FALSE (1==0)

Matrix extraction (Matrix a, int i, int j)
{
   int k,l ;
   E val ;
   int n = a.nb_rows ;

   Matrix b = newMatrix(n-1,n-1) ;

   for (k = 1 ; k <= (i-1) ; k++)
   {
   		for (l = 1 ; l <= (j-1) ; l++)
   		{
   			val = getElt(a,k,l) ;
   			setElt(b,k,l,val) ;
   		}
   }

   for (k = 1 ; k <= (i-1) ; k++)
   {
   		for (l = (j+1) ; l <= n ; l++)
   		{
   			val = getElt(a,k,l) ;
   			setElt(b,k,l-1,val) ;
   		}
   }

   for (k = (i+1) ; k <= n ; k++)
   {
   		for (l = 1 ; l <= (j-1) ; l++)
   		{
   			val = getElt(a,k,l) ;
   			setElt(b,k-1,l,val) ;
   		}
   }

   for (k = (i+1) ; k <= n ; k++)
   {
   		for (l = (j+1) ; l <= n ; l++)
   		{
   			val = getElt(a,k,l) ;
   			setElt(b,k-1,l-1,val) ;
   		}
   }

   return b ;
}

E determinant (Matrix a, int n)
{
  Matrix submatrix ;
  E sign = 1. ;
  E det = 0. ;
  int i ;

  if (isSquare(a) == FALSE)
  {
    fprintf(stderr, "Matrix is not square.\n");
    det = -1. ;
  }

  else
  {
    if (n == 1)
    {
      det = getElt(a,1,1) ;
    }

    else if (n == 2)
    {
      det = getElt(a,1,1) * getElt(a,2,2) - getElt(a,1,2) * getElt(a,2,1) ;
    }

    else
    {
      for (i = 1 ; i <= n ; i++)
      {
        submatrix = extraction(a,i,1) ;
        det+=getElt(a,i,1) * sign * determinant(submatrix,n-1) ;
        sign*=(-1) ;
        deleteMatrix(submatrix) ;
      }
    }
  }

  return det ;
}

Matrix triangular (Matrix a)
{
	Matrix tmpM = copyMatrix(a) ;
	int n = tmpM.nb_rows ;
	int m = tmpM.nb_columns ;
	int k,i,j ;
	E p,q,val ;

	for (k = 1 ; k <= n ; k++)
	{
		p = getElt(tmpM,k,k) ;

		for (i = (k+1) ; i <= n ; i++)
		{
			q = getElt(tmpM,i,k) ;
			setElt(tmpM,i,k,0) ;

			for (j = (k+1) ; j <= m ; j++)
			{
				val = getElt(tmpM,i,j) -(getElt(tmpM,k,j) * q/p) ;
				setElt(tmpM,i,j,val) ;
			}
		}
	}

	return tmpM ;
}

E optimized_determinant(Matrix a)
{
  Matrix trig_a = triangular(a) ;
  Matrix submatrix = extraction(trig_a,1,1) ;
  E d, d_submatrix, val ;

  if (isSquare(a) == FALSE)
  {
    fprintf(stderr, "Matrix is not square.\n");
    d = -1. ;
  }

  else
  {
    val = getElt(trig_a,1,1) ;
    d_submatrix = determinant(submatrix,submatrix.nb_rows) ;
    d = val * d_submatrix ;
  }

  deleteMatrix(trig_a) ;
  deleteMatrix(submatrix) ;
  return d ;
}

int rank (Matrix a)
{
  Matrix trig_a = triangular(a) ;
  int i,j ;
  E val ;
  E my_zero = 1.00 - 1.00 ;

  int r = 0 ;
  int nb_null_values = 0 ;

  for (i = 1 ; i <= a.nb_rows ; i++)
  {
    for (j = 1 ; j <= a.nb_columns ; j++)
    {
      val = getElt(trig_a,i,j) ;

      if (val == my_zero) {nb_null_values++ ;}
    }

    if (nb_null_values < a.nb_columns)
    {
      r++ ;
    }

    nb_null_values = 0 ;
  }

  deleteMatrix(trig_a) ;
  return r ;
}

Matrix inverse (Matrix a)
{
  if (isSquare(a) == FALSE)
  {
    fprintf(stderr, "The matrix is not square. Can't invert.\n");
    return a ;
  }

  E det = determinant(a,a.nb_rows) ;

  if (det == 0.)
  {
    fprintf(stderr, "Determinant is null. Can't invert.\n");
    return a ;
  }

  int i, j ;
  int sign = 1 ;
  int n = a.nb_rows ;

  E inv_det = 1./det ;
  E tmp_det ;

  Matrix t = transpose(a) ;
  Matrix inv = newMatrix(n,n) ;
  Matrix id = identity(n) ;
  Matrix e, mul ;

  for (i = 1 ; i <= n ; i++)
  {
    for (j = 1 ; j <= n ; j++)
    {
      e = extraction(t,i,j) ;
      tmp_det = determinant(e,e.nb_rows) ;
      tmp_det = tmp_det*sign ;
      setElt(inv,i,j,tmp_det) ;
      deleteMatrix(e) ;

      sign*=(-1) ;
    }
  }

  inv = mult_scalar(inv,inv_det) ;
  mul = multiplication(a,inv) ;

  if (matrix_equality(id,mul) == TRUE)
  {
    printf("A * inv(A) = identity --> Inverse is correct.\n");
  }

  deleteMatrix(mul) ;
  deleteMatrix(id) ;
  deleteMatrix(t) ;

  return inv ;
}


void LU_decomposition(Matrix a)	// with Doolittle's Method
{
  int i,j,k ;
	int n = a.nb_columns ;
	E val, sum ;

  Matrix l = newMatrix(n,n) ;
	Matrix u = newMatrix(n,n) ;
  Matrix mul ;

  fill_matrix(l,0.) ;
  fill_matrix(u,0.) ;

	if (isSquare(a) == FALSE)
	{
		fprintf(stderr, "\nThe matrix is not square. LU Decomposition impossible.\n");
	}

  else
  {
    for (i = 1 ; i <= n ; i++)
    {
      for (k = i ; k <= n ; k++)
      {
        sum = 0. ;
        for (j = 1 ; j <= i ; j++)
        {
          sum+=getElt(l,i,j)*getElt(u,j,k) ;
        }

        val = getElt(a,i,k) - sum ;
        setElt(u,i,k,val) ;
      }

      for (k = i ; k <= n ; k++)
      {
        if (i==k)
        {
          setElt(l,i,i,1.) ;
        }

        else
        {
          sum = 0. ;

          for (j = 1 ; j <= i ; j++)
          {
            sum+=getElt(l,k,j)*getElt(u,j,i) ;
          }

          val = (getElt(a,k,i) - sum)/getElt(u,i,i) ;
          setElt(l,k,i,val) ;
        }
      }
    }

	  printf("\nL\n");
    print_matrix(l) ;

	  printf("\nU\n");
	  print_matrix(u) ;

    printf("\nTest A = LU\n");
    mul = multiplication(l,u) ;

    if (matrix_equality(a,mul) == TRUE)
    {
      printf("A = LU --> Decomposition is correct.\n");
    }
  }

	deleteMatrix(l) ;
	deleteMatrix(u) ;
  deleteMatrix(mul) ;

	return ;
}
