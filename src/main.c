#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/matrix.h"
#include "../include/operations.h"
#include "../include/advanced_operations.h"
#include "../include/system_solver.h"
#include "../include/block.h"
#include "../include/main_backup_functions.h"

#define TRUE (1==1)
#define FALSE (1==0)

int main()
{
  Matrix a = setMatrixA() ;
  Matrix b = setMatrixB() ;
  Matrix c = setMatrixC() ;
  Matrix d = setMatrixD() ;
  Matrix e = setMatrixE() ;
  Matrix f = setMatrixF() ;
  Matrix transp_a, transp_b, result, blo ;

  printf("Display A\n");
  print_matrix(a) ;

  printf("Display B\n");
  print_matrix(b);

  printf("A carrée ?\n");
  test_Square(a) ;

  printf("B carrée ?\n");
  test_Square(b) ;

  printf("A symétrique ?\n");
  test_Symetric(a) ;

  printf("B symétrique ?\n");
  test_Symetric(b) ;

  printf("Transpose(A)\n");
  transp_a = transpose(a) ;
  print_matrix(transp_a) ;

  printf("Transpose(B)\n");
  transp_b = transpose(b) ;
  print_matrix(transp_b) ;

  printf("A+B\n");
  result = addition(a,b) ;
  print_matrix(result);

  printf("B+A\n");
  result = addition(b,a) ;
  print_matrix(result);

  printf("A*B\n");
  result = multiplication(a,b) ;
  print_matrix(result);

  printf("B*A\n");
  result = multiplication(b,a) ;
  print_matrix(result);

  printf("t(A)*B\n");
  result = multiplication(transp_a,b) ;
  print_matrix(result);

  printf("A*t(B)\n");
  result = multiplication(a,transp_b) ;
  print_matrix(result);

  printf("A + t(A)\n");
  result = addition(a,transp_a) ;
  print_matrix(result);

  printf("5*A\n");
  result = mult_scalar(a,5.) ;
  print_matrix(result) ;

  printf("3*B\n");
  result = mult_scalar(b,3.) ;
  print_matrix(result) ;

  printf("A + t(A) symetric ?\n");
  result = addition(a,transp_a) ;
  test_Symetric(result) ;

  printf("\nBlocks\n");

  printf("Display D\n");
  print_matrix(d) ;

  printf("Display E\n");
  print_matrix(e);

  printf("Block(d,1,2,e)\n");
  blo = setMatrixBlock(d,1,2,e) ;
  print_matrix(blo) ;

  printf("Display f\n");
  print_matrix(f) ;

  printf("getBlock(f,1,2,2,3)\n");
  blo = getMatrixBlock(f,1,2,2,3) ;
  print_matrix(blo) ;

  printf("\ndet(A) = %.2f\n",determinant(a,a.nb_rows)) ;
  printf("\ndet(B) = %.2f\n",determinant(b,b.nb_rows)) ;

  printf("\nRang(A) = %d\n",rank(a));
  printf("Rang(B) = %d\n",rank(b));

  printf("\ndet(A + t(A)) = %.2f\n",determinant(result,result.nb_rows)) ;

  printf("\nSystem solver\n");
  solve_system(a,c) ;

  printf("\nInverse(A)\n");
  result = inverse(a) ;
  print_matrix(result) ;


  printf("\nLU Decomposition(A)\n");
  LU_decomposition(a) ;

  //Delete all Matrixes
  deleteMatrix(a) ;
  deleteMatrix(b) ;
  deleteMatrix(c) ;
  deleteMatrix(d) ;
  deleteMatrix(e) ;
  deleteMatrix(f) ;
  deleteMatrix(transp_a) ;
  deleteMatrix(transp_b) ;
  deleteMatrix(result) ;
  deleteMatrix(blo) ;

  return 0 ;
}
