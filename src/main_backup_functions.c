#include <stdio.h>
#include <stdlib.h>
#include "../include/matrix.h"
#include "../include/operations.h"
#include "../include/main_backup_functions.h"

#define TRUE (1==1)
#define FALSE (1==0)

Matrix setMatrixA()
{
  Matrix a = newMatrix(3,3) ;
  setElt(a,1,1,1.) ;
  setElt(a,1,2,3.) ;
  setElt(a,1,3,5.) ;

  setElt(a,2,1,2.) ;
  setElt(a,2,2,5.) ;
  setElt(a,2,3,1.) ;

  setElt(a,3,1,-1.) ;
  setElt(a,3,2,-4.) ;
  setElt(a,3,3,-3.) ;

  return a ;
}

Matrix setMatrixB()
{
  Matrix b = newMatrix(2,3) ;
  setElt(b,1,1,1.) ;
  setElt(b,1,2,4.) ;
  setElt(b,1,3,2.) ;

  setElt(b,2,1,2.) ;
  setElt(b,2,2,5.) ;
  setElt(b,2,3,1.) ;

  return b ;
}

Matrix setMatrixC()
{
  Matrix c = newMatrix(3,1) ;
  setElt(c,1,1,0.) ;
  setElt(c,2,1,-7.) ;
  setElt(c,3,1,4.) ;

  return c  ;
}

Matrix setMatrixD()
{
  Matrix d = newMatrix(3,3) ;
  setElt(d,1,1,1.) ;
  setElt(d,1,2,3.) ;
  setElt(d,1,3,5.) ;

  setElt(d,2,1,2.) ;
  setElt(d,2,2,5.) ;
  setElt(d,2,3,1.) ;

  setElt(d,3,1,-1.) ;
  setElt(d,3,2,-4.) ;
  setElt(d,3,3,-3.) ;

  return d ;
}

Matrix setMatrixE()
{
  Matrix e = newMatrix(2,2) ;
  setElt(e,1,1,10.) ;
  setElt(e,1,2,20.) ;

  setElt(e,2,1,30.) ;
  setElt(e,2,2,40.) ;

  return e ;
}

Matrix setMatrixF()
{
  Matrix f = newMatrix(2,3) ;

  setElt(f,1,1,1.) ;
  setElt(f,1,2,3.) ;
  setElt(f,1,3,5.) ;

  setElt(f,2,1,2.) ;
  setElt(f,2,2,5.) ;
  setElt(f,2,3,1.) ;

  return f ;
}

void test_Symetric(Matrix a)
{
  if (isSymetric(a) == TRUE)
  {
    printf("TRUE\n");
  }

  else
  {
    printf("FALSE\n");
  }

  printf("\n");
  return ;
}

void test_Square(Matrix a)
{
  if (isSquare(a) == TRUE)
  {
    printf("TRUE\n");
  }

  else
  {
    printf("FALSE\n");
  }

  printf("\n");
  return ;
}
